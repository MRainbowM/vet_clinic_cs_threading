import json 
import socket
import os
import random
import string
import datetime
import pymysql
import redis

from sys import exit
import win32api
import win32event

import time

import threading

import winerror
# from threading import Thread

import sqlalchemy
from sqlalchemy import orm, and_, text, func
from models import *

from sqlalchemy.dialects.mysql import insert

POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=2)
engine = sqlalchemy.create_engine('mysql+pymysql://maria:1234@localhost/vc_2')

def start_server():
    serv_sock = create_serv_sock(8080)
    cid = 0
    while True:
        client = accept_client_conn(serv_sock, cid)
        client_sock = client[0]
        client_addr = client[1]
        t = threading.Thread(target=serve_client, args=(client_sock, cid, client_addr))
        t.start()
        cid += 1

def create_serv_sock(serv_port):
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,  proto=0)
    serv_sock.bind(('', serv_port))
    serv_sock.listen()
    print('Сервер запущен')
    return serv_sock

def accept_client_conn(serv_sock, cid):
    client_sock, client_addr = serv_sock.accept()
    print(f'Клиент #{cid} подключен ' f'{client_addr[0]}:{client_addr[1]}')
    return client_sock, client_addr

def serve_client(client_sock, cid, client_addr):
    while True:
        answer = fulfill_request(client_sock, client_addr, cid)
        if answer is None:
            print(f'Клиент #{cid} преждевременно отключился')
            break
        else:
            client_sock.send(bytes(json.dumps(answer), 'UTF-8')) # Отправка данных клиенту

def fulfill_request(client_sock, client_addr, cid):
    try:
        in_data = client_sock.recv(4096)# Получение данных от клиента
    except:
        return None 
        
    msg = in_data.decode() # Декодирование данных от клиента
    data = json.loads(msg) # Преобразование данных из формата JSON в словарь Python
    answer = None 
    if data["command"] == 'bye': 
        clear_token(data["token"])
        print(f'Клиент #{cid} отключился')
        logging(cid, client_addr, datetime.datetime.now(), 'bye')
        client_sock.close() 

    if data["command"] == 'stop': #УБРАТЬ ??!!
        print("Отключаем сервер")
        clear_token(data["token"])
        client_sock.close() 
        # server.close() 
        exit(0) 

    if data["command"] == 'login_verification': 
        # print("Проверка логина")
        logging(cid, client_addr, datetime.datetime.now(), 'login_verification')
        answer = login_verification(data["login"])

    if data["command"] == 'pass_verification': 
        # print("Проверка пароля")
        logging(cid, client_addr, datetime.datetime.now(), 'pass_verification')
        answer = pass_verification(data["login"], data["password"])

    if data["command"] == 'get_all_services': 
        # print("Список всех услуг")
        logging(cid, client_addr, datetime.datetime.now(), 'get_all_services')
        answer = get_all_services()

    if data["command"] == 'get_service': 
        # print("Поиск улуги по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_service')
        answer = get_service(data["object"])
        
    if data["command"] == 'get_all_filials': 
        # print("Список всех филиалов")
        logging(cid, client_addr, datetime.datetime.now(), 'get_all_filials')
        answer = get_all_filials()

    if data["command"] == 'get_filial': 
        # print("Поиск филала по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_filial')
        answer = get_filial(data["object"])

    if data["command"] == 'get_all_cabinets': #команда не нужна??
        # print("Список всех кабинетов")
        # logging(cid, client_addr, datetime.datetime.now(), 'get_filial')
        # answer = get_all_cabinets()
        answer = {}

    if data["command"] == 'get_cabinet': 
        print("Поиск кабинета по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_cabinet')
        answer = get_cabinet(data["object"])

    if data["command"] == 'get_all_kinds': 
        # print("Список всех видов животных")
        logging(cid, client_addr, datetime.datetime.now(), 'get_all_kinds')
        answer = get_all_kinds()

    if data["command"] == 'get_kind': 
        # print("Поиск вида животного по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_kind')
        answer = get_kind(data["object"])

    if data["command"] == 'get_workers_by_service': 
        # print("Поиск сотрудников по услуге")
        logging(cid, client_addr, datetime.datetime.now(), 'get_workers_by_service')
        answer = get_workers_by_service(data["object"])

    if data["command"] == 'get_worker': 
        # print("Поиск сотрудника по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_worker')
        answer = get_worker(data["object"])

    if data["command"] == 'add_client': 
        # print("Регистрация нового пользователя")
        logging(cid, client_addr, datetime.datetime.now(), 'add_client')
        answer = add_client(data["object"])

    if data["command"] == 'get_client_by_user': 
        # print("Поиск клиента по пользователю")
        logging(cid, client_addr, datetime.datetime.now(), 'get_client_by_user')
        answer = get_client_by_user(data["token"])

    if data["command"] == 'save_client': 
        # print("Сохранение изменений клиента")
        logging(cid, client_addr, datetime.datetime.now(), 'save_client')
        answer = save_client(data["token"], data["object"])

    if data["command"] == 'add_pet': 
        # print("Добавление питомца")
        logging(cid, client_addr, datetime.datetime.now(), 'add_pet')
        answer = add_pet(data["token"], data["object"])

    if data["command"] == 'get_pets_by_client': 
        # print("Поиск питомцев по клиенту")
        logging(cid, client_addr, datetime.datetime.now(), 'get_pets_by_client')
        answer = get_pets_by_client(data["token"])

    if data["command"] == 'get_pet': 
        # print("Поиск питомца по id")
        logging(cid, client_addr, datetime.datetime.now(), 'get_pet')
        answer = get_pet(data["token"], data["object"])

    if data["command"] == 'save_pet': 
        # print("Сохранить изменения питомца")
        logging(cid, client_addr, datetime.datetime.now(), 'save_pet')
        answer = save_pet(data["token"], data["object"])

    if data["command"] == 'del_pet': 
        # print("Удаление питомца")
        logging(cid, client_addr, datetime.datetime.now(), 'del_pet')
        answer = del_pet(data["token"], data["object"])

    if data["command"] == 'get_visit_by_client': 
        # print("Поиск приемов клиента")
        logging(cid, client_addr, datetime.datetime.now(), 'get_visit_by_client')
        answer = get_visit_by_client(data["token"])

    if data["command"] == 'cancel_visit': 
        # print("Отмена приема")
        logging(cid, client_addr, datetime.datetime.now(), 'cancel_visit')
        answer = cancel_visit(data["token"], data["object"])

    if data["command"] == 'get_visit_by_pet': 
        # print("Поиск приемов питомца")
        logging(cid, client_addr, datetime.datetime.now(), 'get_visit_by_pet')
        answer = get_visit_by_pet(data["token"], data["object"])

    if data["command"] == 'get_free_date_schedule': 
        # print("Поиск дат для записи на прием")
        logging(cid, client_addr, datetime.datetime.now(), 'get_free_date_schedule')
        answer = get_free_date_schedule(data["filial_id"], data["worker_id"])

    if data["command"] == 'get_free_time_schedule': 
        # print("Поиск времени для записи на прием")
        logging(cid, client_addr, datetime.datetime.now(), 'get_free_time_schedule')
        answer = get_free_time_schedule(data["schedule_id"], data["services"])

    if data["command"] == 'add_visit': 
        # print("Создание записи на прием")
        logging(cid, client_addr, datetime.datetime.now(), 'add_visit')
        answer = add_visit(data["token"], data["object"])

    return answer

#####################################################################

class WinMutex:
    def __init__(self, name):
        self.mutexname = name
        self.mutex = win32event.CreateMutex(None, 1, self.mutexname)
        self.lasterror = win32api.GetLastError()

    def release(self):
        return win32event.ReleaseMutex(self.mutex)

    def wait_mutex(self, mutex):
        win32event.WaitForSingleObject(mutex, win32event.INFINITE)  



def logging(cid, client_addr, ev_date, ev_name): #запись в журнал
    event = f'client #{cid} - {client_addr[0]}:{client_addr[1]} ' + ev_name
    row = str(ev_date) + ' ' + event + '\n'
    mutex = WinMutex("mutex_vet_clinic")
    while True:
        mutex.wait_mutex(mutex.mutex)
        print(f'client #{cid} зашел')
        time.sleep(3)
        event_log = open('event_log.txt', 'a')
        event_log.write(row)
        event_log.close()
        print(f'client #{cid} вышел')
        mutex.release()
        return
    
#####################################################################

def clear_token(token):
    Session = sessionmaker(bind=engine)
    session = Session()
    user_q = session.query(User).filter_by(token = token).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id)
    if not user: 
        print("(!)Ошибка")
        return {}
 
    update_stmt = user_table.update().values(token = None).where(user_table.c.id == user["id"])
    conn = engine.connect()
    result = conn.execute(update_stmt)
    return token

def login_verification(login):
    Session = sessionmaker(bind=engine)
    session = Session()
    user_q = session.query(User).filter_by(username = login).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id)
    if not user: 
        # print("(!)Ошибка: неправильный логин")
        return -1
    else:
        return user["id"]

def pass_verification(login, pwd):
    Session = sessionmaker(bind=engine)
    session = Session()
    user_q = session.query(User).filter_by(username = login, password = pwd).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id)
    if not user: 
        # print("(!)Ошибка: неправильный логин или пароль")
        return {}
    token = ""
    
    token = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(30))

    update_stmt = user_table.update().values(token = token).where(user_table.c.id == user["id"])
    conn = engine.connect()
    result = conn.execute(update_stmt)
    return token

#SERVICE#############################################################

def get_all_services():
    cache = get_cache("get_all_services", 0)
    try:
        return cache["get_all_services"]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        services_q = session.query(Service).filter_by(visibility = True, date_of_delete = None).all()
        services = {}
        for row in services_q:
            service = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                duration = row.duration,
                cost = row.cost,
                nurse = row.nurse)
            key = "service " + str(row.id)
            services[key] = service
        if not services:
            # print("услуги не найдены")
            return {}
        elif service:
            set_cache("get_all_services", services, cache["set"])
            return services

def get_service(id):
    cache = get_cache("get_service", id)
    try:
        return cache["get_service " + str(id)]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        service_q = session.query(Service).filter_by(id = id).all()
        for row in service_q:
            service = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                duration = row.duration,
                cost = row.cost,
                nurse = row.nurse)
        if not service: 
            # print("(!)Ошибка: услуга не найдена")
            return {}
        elif service:
            set_cache("get_service " + str(id), service, cache["set"])
            return service
    
#Filial##############################################################

def get_all_filials():
    cache = get_cache("get_all_filials", 0)
    try:
        return cache["get_all_filials"]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        filials_q = session.query(Filial).filter_by(visibility = True, date_of_delete = None).all()
        filials = {}
        for row in filials_q:
            filial = dict(
                id = row.id,
                address_full = row.address_full,
                address = row.address,
                mail = row.mail)
            key = "filial " + str(row.id)
            filials[key] = filial
        if not filials:
            print("(!)Ошибка: филиалы не найдены")
            return {}
        elif filial:
            set_cache("get_all_filials", filials, cache["set"])
            return filials

def get_filial(id):
    cache = get_cache("get_filial", id)
    try:
        return cache["get_filial " + str(id)]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        filials_q = session.query(Filial).filter_by(id = id).all()
        for row in filials_q:
            filial = dict(
                id = row.id,
                address_full = row.address_full,
                address = row.address,
                mail = row.mail)
        if not filial:
            print("(!)Ошибка: филиал не найден")
            return {}
        elif filial:
            set_cache("get_filial " + str(id), filial, cache["set"])
            return filial

#CABINET#############################################################

#не нужна? ПЕРЕДЕЛАТЬ ПО ФИЛИАЛУ либо браьт из расписания
def get_cabinets_by_schedule(filial_id, date, worker_id):
    date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    cabinet = 0
    # try:
    Session = sessionmaker(bind=engine)
    session = Session()
    schedule_q = session.query(Schedule).filter_by(filial_id = filial_id, worker_id = worker_id).all()
    for row in schedule_q:
        if (row.time1_start != None) and (row.time1_start < date) and (row.time1_end > date):
            return row.cabinet_id
        elif (row.time2_start != None) and (row.time2_start < date) and (row.time2_end > date):
            return row.cabinet_id
        elif (row.time3_start != None) and (row.time3_start < date) and (row.time3_end > date):
            return row.cabinet_id
    return cabinet
    # except:
    #     print("(!)Ошибка: кабинет не найден")
    #     return {}

def get_cabinet(id):
    cache = get_cache("get_cabinet", id)
    try:
        return cache["get_cabinet " + str(id)]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        cabinet_q = session.query(Cabinet).filter_by(id = id).all()
        for row in cabinet_q:
            cabinet = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                filial_id = row.filial_id)
        if not cabinet: 
            print("(!)Ошибка: кабинет не найден")
            return {}
        elif cabinet:
            set_cache("get_cabinet " + str(id), cabinet, cache["set"])
            return cabinet

#KIND################################################################

def get_all_kinds():
    cache = get_cache("get_all_kinds", 0)
    try:
        return cache["get_all_kinds"]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        kinds_q = session.query(Kind).all()
        kinds = {}
        for row in kinds_q:
            kind = dict(
                id = row.id,
                value = row.value)
            key = "kind " + str(row.id)
            kinds[key] = kind
        if not kinds:
            print("(!)Ошибка: виды питомцев не найдены")
            return {}
        elif kind:
            set_cache("get_all_kinds", kinds, cache["set"])
            return kinds

def get_kind(id):
    cache = get_cache("get_kind", id)
    try:
        return cache["get_kind " + str(id)]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        kind_q = session.query(Kind).filter_by(id = id).all()
        for row in kind_q:
            kind = dict(
                id = row.id,
                value = row.value)
        if not kind: 
            print("(!)Ошибка: вид питомца не найден")
            return {}
        elif kind:
            set_cache("get_kind " + str(id), kind, cache["set"])
            return kind

#WORKER##############################################################

def get_workers_by_service(service_id):
    cache = get_cache("get_workers_by_service", service_id)
    try:
        return cache["get_workers_by_service " + service_id]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        worker_q = session.query(Worker).join(Worker_services, Worker_services.worker_id == Worker.id).filter_by(service_id = service_id).all()
        workers = {}
        for row in worker_q:
            worker = dict(
                id = row.id,
                surname = row.surname,
                name = row.name,
                patronymic = row.patronymic,
                phone = row.phone,
                mail = row.mail,
                date_of_birth = row.date_of_birth,
                info = row.info,
                user_id = row.user_id,
                position_id = row.position_id)
            key = "worker " + str(row.id)
            workers[key] = worker
        if not workers: 
            print("работник не найден")
            return {}
        elif workers:
            set_cache("get_workers_by_service " + str(service_id), workers, cache["set"])
            return workers

def get_worker(id):
    cache = get_cache("get_worker", id)
    try:
        return cache["get_worker " + str(id)]
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        worker_q = session.query(Worker).filter_by(id = id).all()
        for row in worker_q:
            worker = dict(
                id = row.id,
                surname = row.surname,
                name = row.name,
                patronymic = row.patronymic,
                phone = row.phone,
                mail = row.mail,
                date_of_birth = row.date_of_birth,
                info = row.info,
                user_id = row.user_id,
                position_id = row.position_id)
        if not worker: 
            print("(!)Ошибка: работник не найден")
            return {}
        elif worker:
            set_cache("get_worker " + str(id), worker, cache["set"])
            return worker

#CLIENT##############################################################

def add_client(user):
    try:
        insert_stmt = insert(User).values(
            username=user['username'],

            password=user['password'])
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        user_id = result.inserted_primary_key
        print("Добавлен пользователь " + str(user['username']))
        try:
            insert_stmt = insert(Client).values(
            user_id = user_id)
            result = conn.execute(insert_stmt)
            print("Добавлен новый клиент")
            return user_id
        except:
            print("(!)Ошибка: клиент не создан")
    except:
        print("(!)Ошибка: пользователь не создан")

def get_client_by_user(token):
    Session = sessionmaker(bind=engine)
    session = Session()
    client_q = session.query(Client).join(User, User.id == Client.user_id).filter_by(token = token).all()
    client = {}
    for row in client_q:
        client = dict(
            id = row.id,
            surname = row.surname,
            name = row.name,
            patronymic = row.patronymic,
            phone = row.phone,
            mail = row.mail,
            photo = row.photo,
            date_of_birth = row.date_of_birth,
            user_id = row.user_id)
    if not client: 
        print("(!)Ошибка: клиент не найден")
        return {}
    elif client:
        return client

def save_client(token, client_edit):
    client = get_client_by_user(token)
    if not client:
        print("(!)Ошибка: информация не сохранена")
        return 
    try:
        if not "surname" in client_edit:
            surname = None
        else:
            surname = client_edit["surname"]
        if not "name" in client_edit:
            name = None
        else:
            name = client_edit["name"]
        if not "patronymic" in client_edit:
            patronymic = None
        else:
            patronymic = client_edit["patronymic"]
        if not "phone" in client_edit:
            phone = None
        else:
            phone = client_edit["phone"]
        if not "mail" in client_edit:
            mail = None
        else:
            mail = client_edit["mail"]
        if not "photo" in client_edit:
            photo = None
        else:
            photo = client_edit["photo"]
        if not "date_of_birth" in client_edit:
            date_of_birth = None
        else:
            date_of_birth = client_edit["date_of_birth"]
        update_stmt = client_table.update().values(surname = surname, name = name, patronymic = patronymic, phone = phone, mail = mail, photo = photo, date_of_birth = date_of_birth).where(client_table.c.id == client["id"])
        conn = engine.connect()
        result = conn.execute(update_stmt)
        print("Клиент изменен")
        return client["id"]
    except:
        print("Ошибка, информация не сохранена")
        return {}

#PET#################################################################
    
def add_pet(token, pet):
    client = get_client_by_user(token)
    try:
        if not "date_of_birth" in pet:
            date_of_birth = None
        if not "name" in pet:
            name = None
        if not "photo" in pet:
            photo = None
        if not "sex" in pet:
            sex = 1
        if not "kind_id" in pet:
            kind_id = None
        insert_stmt = insert(Pet).values(
            name = name,
            sex = sex,
            photo = photo,
            date_of_birth = date_of_birth,
            kind_id = kind_id,
            client_id = client['id'])
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        pet_id = result.inserted_primary_key
        print("Добавлен питомец " + str(pet_id))
        return pet_id
    except:
        print("(!)Ошибка: питомец не создан")

def get_pets_by_client(token):
    Session = sessionmaker(bind=engine)
    session = Session()
    client = get_client_by_user(token)
    pet_q = session.query(Pet).filter_by(client_id = client["id"]).all()
    pets = {}
    for row in pet_q:
        kind = get_kind(row.kind_id)
        pet = dict(
            id = row.id,
            name = row.name,
            sex = row.sex,
            date_of_birth = row.date_of_birth,
            kind = kind["value"],
            client = row.client_id)
        key = "pet " + str(row.id)
        pets[key] = pet
    if not pets:
        print("Питомцы не найдены")
        return {}
    else:
        return pets

def get_pet(token, id):
    client = get_client_by_user(token)
    Session = sessionmaker(bind=engine)
    session = Session()
    pet_q = session.query(Pet).filter_by(id = id).all()
    pet = {}
    for row in pet_q:
        kind = get_kind(row.kind_id)
        pet = dict(
            id = row.id,
            name = row.name,
            sex = row.sex,
            date_of_birth = row.date_of_birth,
            kind = kind["value"],
            client = row.client_id)
    if not client: 
        print("(!)Ошибка: клиент не найден")
        return {}
    if not pet: 
        print("(!)Ошибка: питомец не найден")
        return {}
    elif pet["client"] != client["id"]:
        print("(!)Ошибка доступа: питомец не принадлежит клиенту")
        return {}
    elif pet["client"] == client["id"]:
        return pet

def save_pet(token, pet):
    client = get_client_by_user(token)
    try:
        if not "date_of_birth" in pet:
            date_of_birth = None
        else:
            date_of_birth = pet["date_of_birth"]
        if not "name" in pet:
            name = None
        else:
            name = pet["name"]
        if not "photo" in pet:
            photo = None
        else:
            photo = pet["photo"]
        if not "sex" in pet:
            sex = 1
        else:
            sex = pet["sex"]
        if not "kind_id" in pet:
            kind_id = None
        else:
            kind_id = pet["kind_id"]

        update_stmt = pet_table.update().values(name = name, sex = sex, date_of_birth = date_of_birth, photo = photo).where(and_(pet_table.c.id == pet["id"], pet_table.c.client_id == client["id"]))
        conn = engine.connect()
        result = conn.execute(update_stmt)
        print("Питомец изменен")
        return pet
    except:
        print("Ошибка, информация не сохранена")
        return {}

def del_pet(token, pet_id):
    client = get_client_by_user(token)
    try:
        update_stmt = pet_table.update().values(date_of_delete = datetime.date.today()).where(and_(pet_table.c.id == pet_id, pet_table.c.client_id == client["id"]))
        conn = engine.connect()
        result = conn.execute(update_stmt)
        print("Питомец удален")
        return pet_id
    except:
        print("Ошибка, информация не сохранена")
        return {}

#VISIT###############################################################

def get_visit_by_client(token):
    client = get_client_by_user(token)
    if not client: 
        print("(!)Ошибка: клиент не найден")
        return {}
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'])
    visits = {}
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)
        doctor = get_worker(row.doctor_id)
        doctor = doctor["surname"] + " " + doctor["name"] + " " + doctor["patronymic"]
        filial = get_filial(row.filial_id)
        filial1 = ""
        filial1 = filial["address"]
        pet = get_pet(token, row.pet_id)
        pet1 = pet["name"]
        cabinet = cabinet["name"]
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = filial1,
            pet = pet1,
            doctor = doctor)
        key = "visit " + str(row.id)
        visits[key] = visit
    return visits

def cancel_visit(token, id):
    client = get_client_by_user(token)
    if not client:
        print("Ошибка, информация не сохранена")
        return {}
    try:
        update_stmt = visit_table.update().values(status = 2).where(and_(visit_table.c.id == id, visit_table.c.client_id == client["id"]))
        conn = engine.connect()
        result = conn.execute(update_stmt)
        print("Прием отменен")
        return id
    except:
        print("Ошибка, информация не сохранена")
        return {}
    
def get_visit_by_pet(token, id):
    client = get_client_by_user(token)
    if not client: 
        print("(!)Ошибка: клиент не найден")
        return {}
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'], pet_id = id, date_of_delete = None)
    visits = {}
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)
        doctor = get_worker(row.doctor_id)
        doctor = doctor["surname"] + " " + doctor["name"] + " " + doctor["patronymic"]
        filial = get_filial(row.filial_id)
        filial1 = ""
        filial1 = filial["address"]
        pet = get_pet(token, row.pet_id)
        pet1 = pet["name"]
        cabinet = cabinet["name"]
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = filial1,
            pet = pet1,
            doctor = doctor)
        key = "visit " + str(row.id)
        visits[key] = visit
    return visits

#SCHEDULE############################################################

def get_free_date_schedule(filial_id, worker_id): 
    Session = sessionmaker(bind=engine)
    session = Session()
    ###сейчас + 1 час
    schedules_q = session.query(Schedule).filter_by(filial_id = filial_id, worker_id = worker_id).all()
    dates = {}
    for row in schedules_q:
        date = row.time1_start.date()
        dates[row.id] = str(date)
    return dates

def get_free_time_schedule(schedule_id, services):# services - [id]
    duration = 0
    for service_id in services:
        service = get_service(service_id)
        duration = duration + int(service["duration"])

    Session = sessionmaker(bind=engine)
    session = Session()
    schedules_q = session.query(Schedule).filter_by(id = schedule_id).all()

    for row in schedules_q:
        schedule = dict(
            id = row.id,
            worker_id = row.worker_id,
            # filial_id = row[2],
            # cabinet_id = row[3],
            time1_start = row.time1_start,
            time1_end = row.time1_end,
            time2_start = row.time2_start,
            time2_end = row.time2_end,
            time3_start = row.time3_start,
            time3_end = row.time3_end)
            # comment = row[10])
        date = row.time1_start.date()



    visits_q = session.query(Visit).filter(func.date(Visit.date) == date).all()
    print(visits_q)
    visits = {}
    for row in visits_q:
        date_end = row.date + datetime.timedelta(minutes = row.duration)
        visit = dict(
            doctor_id = row.doctor_id,
            date_start = row.date,
            date_end = date_end)
        key = row.id
        visits[key] = visit



    
    # dt = schedule["time1_start"] - datetime.timedelta(minutes = 20)  
    for visit in visits:
        for v in visits:
            if visits[visit]["date_start"] < visits[v]["date_start"]:
                vvv = visits[v]
                visits[v] = visits[visit]
                visits[visit] = vvv
    # duration
    times = {}
    time = {}
    dt = schedule["time1_start"]  
    i = 1
    
    for visit in visits:
        if visits[visit]["doctor_id"] == schedule["worker_id"]:
            while True:
                if dt >= visits[visit]["date_start"] and dt <= visits[visit]["date_end"]:
                    dt = visits[visit]["date_end"] + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
                    break
                else: 
                    time["date_end"] = str(dt) #end
                    time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
                    times[i] = time.copy()
                    i += 1
                    dt = dt + datetime.timedelta(minutes = 5)


    while dt <= schedule["time1_end"]:
        dt = dt + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
        if dt <= schedule["time1_end"]:
            time["date_end"] = str(dt) #end
            time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
            times[i] = time.copy()
            i += 1


        

    return times

def add_visit(token, visit):
    client = get_client_by_user(token)
    if not client: 
        print("(!)Ошибка: прием не создан")
        return {}
    cost = 0
    duration = 0
    for service in visit["services"]:
        s = get_service(service)
        cost += s["cost"]
        duration += s["duration"]
        workers = get_workers_by_service(service)
        worker = "worker " + str(visit["doctor_id"])
        if not worker in workers:
            print("(!)Ошибка: работник не оказывает данную услугу")
            return {}
    pet = get_pet(token, visit["pet_id"])
    if pet["client"] != client["id"]:
        print("(!)Ошибка: питомец не принадлежит клиенту, прием не создан")
        return {}
   
    cabinet_id = get_cabinets_by_schedule(visit["filial_id"], visit["date"]["date_start"], visit["doctor_id"]) 
    if not cabinet_id:
        print("(!)Ошибка: кабинет не найден, прием не создан")
        return {}
    try:
        insert_stmt = insert(Visit).values(
            date = visit["date"]["date_start"],
            cost = cost,
            filial_id = visit["filial_id"],
            doctor_id = visit["doctor_id"],
            pet_id = visit["pet_id"],
            client_id = client["id"],
            status = 0,
            duration = duration,
            cabinet_id = cabinet_id)
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        user_id = result.inserted_primary_key
        print("Добавлен прием")
        return visit
    except:
        print("(!)Ошибка: прием не создан")
        return {}    

#CACHE###############################################################

def get_cache(request_name, id):
    data = ''
    answer = {}
    answer = dict(set = True)
    try:
        my_server = redis.Redis(connection_pool=POOL)
        if id == 0:
            data = my_server.get(request_name)
        else:
            data = my_server.get(request_name + " " + str(id))
    except:
        answer["set"] = False
        print("Ошибка подключения к Redis")
    if data != "" and data != None:
        print("Берем данные из кэша")
        if id == 0:
            answer[request_name] = json.loads(data)
        else:
            answer[request_name + ' ' + str(id)] = json.loads(data)

        answer["set"] = False

    return answer

def set_cache(request_name, answer, state_set):
    if state_set:
        try:
            my_server = redis.Redis(connection_pool=POOL)
            with my_server.pipeline() as pipe:
                pipe.set(request_name, bytes(json.dumps(answer), 'UTF-8'))
                pipe.execute()
                my_server.bgsave()
                print("Данные добавлены в кэш")
        except:
            print("(!)Ошибка подключения к Redis")
    else:
        print("(!)Данне не закэшированы")

#####################################################################

class WinSemaphore:
    def __init__(self, InitialCount, MaximumCount, SemaphoreName):
        self.SemaphoreName = SemaphoreName
        self.InitialCount = InitialCount
        self.MaximumCount = MaximumCount

        self.semaphore = win32event.CreateSemaphore(None, self.InitialCount, self.MaximumCount, self.SemaphoreName)

    def acquire(self):
        return win32event.WaitForSingleObject(self.semaphore, 100)
        
if __name__ == '__main__':
    s = WinSemaphore(1, 1, "semaphore_vet_clinic")
    if s.acquire() > 0:
            print("Сервер уже запущен!!!")
            exit(0)

mutex = WinMutex()
mutex.release()
start_server()


# print(get_free_time_schedule(2, [1]))



# visit = dict(
#     date = {},
#     doctor_id = 1,
#     services = [1],
#     pet_id = 1,
#     filial_id = 1)
# visit["date"]["date_start"] = '2020-02-21 11:00:00'
# add_visit("cTZ0qTjtpaKvaKtFNNNeRlaOyvyBYi", visit)



# get_all_filials()


# print(get_visit_by_pet("6Q4CHmwhkKPUIBjuVdmzBwPfylCDlF", 1))
# print(get_worker(1))

# print(get_visit_by_client("MLguavPd8qtgIlwzhJ2gpqNdAq3k2U"))

# print(get_free_date_schedule(1,1))

# print(get_pet("MLguavPd8qtgIlwzhJ2gpqNdAq3k2U", 33))

# print(get_client_by_user("MLguavPd8qtgIlwzhJ2gpqNdAq3k2U"))
# user = dict(
#     username = "qwqwqwqw"
# )
# user["password"] = "dfdfdf"

# # add_client(user)

# pet = dict(
#     id = 1,
#     name = "Месье Шариков 2.0",
#     sex = 1,
#     kind_id = 1)
# print(save_pet("6Q4CHmwhkKPUIBjuVdmzBwPfylCDlF", pet))

# del_pet("6Q4CHmwhkKPUIBjuVdmzBwPfylCDlF", 1)
# client = dict(
#     surname = "Иванов",
#     name = "Иван",
#     patronymic = "Иванович")


# print(save_client("6Q4CHmwhkKPUIBjuVdmzBwPfylCDlF", client))

# print(get_pets_by_client("MLguavPd8qtgIlwzhJ2gpqNdAq3k2U"))







