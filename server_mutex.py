import json 
import socket
import os
import datetime
from sys import exit
import win32api
import win32event

import time

import threading

def start_server():
    serv_sock = create_serv_sock(8080)
    cid = 0
    while True:
        client = accept_client_conn(serv_sock, cid)
        client_sock = client[0]
        client_addr = client[1]
        t = threading.Thread(target=serve_client, args=(client_sock, cid, client_addr))
        t.start()
        cid += 1

def create_serv_sock(serv_port):
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,  proto=0)
    serv_sock.bind(('', serv_port))
    serv_sock.listen()
    print('Сервер запущен')
    return serv_sock

def accept_client_conn(serv_sock, cid):
    client_sock, client_addr = serv_sock.accept()
    print(f'Клиент #{cid} подключен ' f'{client_addr[0]}:{client_addr[1]}')
    return client_sock, client_addr

def serve_client(client_sock, cid, client_addr):
    while True:
        answer = fulfill_request(client_sock, client_addr, cid)
        if answer is None:
            print(f'Клиент #{cid} преждевременно отключился')
            break
        else:
            client_sock.send(bytes(json.dumps(answer), 'UTF-8')) # Отправка данных клиенту

def fulfill_request(client_sock, client_addr, cid):
    try:
        in_data = client_sock.recv(4096)# Получение данных от клиента
    except:
        return None 
        
    msg = in_data.decode() # Декодирование данных от клиента
    data = json.loads(msg) # Преобразование данных из формата JSON в словарь Python
    answer = None 
    logging(cid, client_addr, datetime.datetime.now(), data["command"])
    answer = "получил ответ от сервера"
    return answer

 

#####################################################################

class WinMutex:
    def __init__(self, name):
        self.mutexname = name
        self.mutex = win32event.CreateMutex(None, 1, self.mutexname)
        self.lasterror = win32api.GetLastError()

    def release(self):
        return win32event.ReleaseMutex(self.mutex)

    def wait_mutex(self, mutex):
        win32event.WaitForSingleObject(mutex, win32event.INFINITE)  



def logging(cid, client_addr, ev_date, ev_name): #запись в журнал
    event = f'client #{cid} - {client_addr[0]}:{client_addr[1]} ' + ev_name
    row = str(ev_date) + ' ' + event + '\n'
    mutex = WinMutex("mutex_vet_clinic")
    while True:
        mutex.wait_mutex(mutex.mutex)
        print(f'client #{cid} зашел')
        time.sleep(3)
        event_log = open('event_log.txt', 'a')
        event_log.write(row)
        event_log.close()
        print(f'client #{cid} вышел')
        mutex.release()
        return
    
#####################################################################

class WinSemaphore:
    def __init__(self, InitialCount, MaximumCount, SemaphoreName):
        self.SemaphoreName = SemaphoreName
        self.InitialCount = InitialCount
        self.MaximumCount = MaximumCount

        self.semaphore = win32event.CreateSemaphore(None, self.InitialCount, self.MaximumCount, self.SemaphoreName)

    def acquire(self):
        return win32event.WaitForSingleObject(self.semaphore, 100)
        
if __name__ == '__main__':
    s = WinSemaphore(1, 1, "semaphore_vet_clinic")
    if s.acquire() > 0:
            print("Сервер уже запущен!!!")
            exit(0)
mutex = WinMutex("mutex_vet_clinic")
mutex.release()
start_server()




